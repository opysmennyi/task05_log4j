package log4j;

import org.apache.logging.log4j.*;

public class Log4j_2 {
    private static Logger logger1 = LogManager.getLogger(Log4j_2.class);

    public static void main(String[] args) {

        logger1.trace("This is a trace message");
        logger1.debug("This is a debug message");
        logger1.info("This is an info message");
        logger1.warn("This is a warn message");
        logger1.error("This is an error message");
        logger1.fatal("This is a fatal message");
    }
}